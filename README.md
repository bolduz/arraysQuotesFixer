# Arrays' indexes fixer

### Description
This script was created when, migrating an old client website, I found that the previous developer never minded to quote associative arrays indexes.

In PHP writing ```$array[index]``` is not a real error, because (if not already declared) ```index``` gets interpreted as a string. This can get annoying, and fills Apache's error_log pretty quickly if your website generates a lot of traffic.

### Installation
Just clone this repo with ```git clone https://gitlab.com/bolduz/arraysQuotesFixer.git``` or copy-paste the script and you're ready to go.

### Usage
Run the script specifying the files to process or pipe a list of files:
  1. ```python arraysQuotesFixer.py /var/www/html/*.php```
  2. ```cat filelist.txt | python arraysQuotesFixer.py```
