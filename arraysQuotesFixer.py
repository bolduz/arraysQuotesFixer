import re
import fileinput

pattern = re.compile(r'(\$[^\d][\w_]+)\[([a-z_][_\w]*?)\]', re.IGNORECASE)
for line in fileinput.input(inplace=1):
        line = re.sub(pattern, r"\1['\2']", line)
        print line,